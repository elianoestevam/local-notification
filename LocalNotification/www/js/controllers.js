angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});


})

.controller('NotificacaoCtrl', function($scope,$cordovaLocalNotification) {

    $scope.$on('$ionicView.enter', function () {
        //Lista as notificações...
        $scope.getNotificacoes();
    });

    $scope.getNotificacoes = function () {
        $cordovaLocalNotification.getAll().then(function(notis){
            $scope.notificacoes = notis;            
        });        
    };
    
    $scope.criarNotificacao = function () {  
        
        var alarmTime = new Date();
        alarmTime.setMinutes(alarmTime.getMinutes() + 1);
        
        $cordovaLocalNotification.schedule({
            id: 1,
            title: "Titulo",
            text: "Mensagem",
            at: alarmTime
        }).then(function (result) {
             $scope.getNotificacoes();
        });          
    };
});
